/******************************************************************************
 * Copyright (c) Huawei Technologies Co., Ltd. 2022. All rights reserved.
 * gala-gopher licensed under the Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *     http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
 * PURPOSE.
 * See the Mulan PSL v2 for more details.
 * Author: Siki
 * Create: 2022-09-05
 * Description: system probe just in 1 thread, include tcp/net/iostat/inode
 ******************************************************************************/
#include<stdio.h>
#include<string.h>
#include<unistd.h>
#include<stdlib.h>
#include "system_meminfo.h"
#include "common.h"

#define METRICS_MEMINFO_NAME "system_meminfo"
#define METRICS_MEMINFO_PATH "/proc/meminfo"
static struct system_meminfo_field* meminfo_fields = NULL;

int system_meminfo_init(void)
{
    meminfo_fields = (struct system_meminfo_field*)malloc(TOTAL_DATA_INDEX * sizeof(struct system_meminfo_field));
    if (meminfo_fields == NULL) {
        return -1;
    }
    (void)memset(meminfo_fields, 0, TOTAL_DATA_INDEX * sizeof(struct system_meminfo_field));
    // assign key to indicators.
    char key_[TOTAL_DATA_INDEX][KEY_BUF_LEN] = {"MemTotal", "MemFree", "MemAvailable", "Buffers", "Cached",
        "Active", "Inactive", "SwapTotal", "SwapFree"};
    for (int i = MEM_TOTAL; i < TOTAL_DATA_INDEX; i++) {
        strcpy(meminfo_fields[i].key, key_[i]);
    }
    return 0;
}

// destry the memory spce for meminfos.
void system_meminfo_destroy(void)
{
    if (meminfo_fields != NULL) {
        (void)free(meminfo_fields);
        meminfo_fields = NULL;
    }
}

// get key & value from the line text, and assign to the target key.
static int get_meminfosp_fileds(const char* line, const int cur_index)
{
    int ret = 0;

    char* colon = strchr(line, ':');
    if (colon == NULL) {
        return -1;
    }

    *colon = '\0';

    if (strcmp(line, meminfo_fields[cur_index].key) == 0) {
        // atoll() turns digit chars to longlong ignoring the letter chars.  
        meminfo_fields[cur_index].value = atoll(colon + 1);
        return 0;
    }

    return -1;
}


static int output_info(void)
{
    //  v3.2.8 used = total - free; v3.3.10 used = total - free - cache - buffers; cur_ver=v5.10
    // alculate memusage
    double mem_usage = (double)(meminfo_fields[MEM_TOTAL].value - meminfo_fields[MEM_FREE].value - \
                    meminfo_fields[BUFFERS].value - meminfo_fields[CACHED].value) / meminfo_fields[MEM_TOTAL].value;

    // calculate swapusage
    double swap_usage = (double)(meminfo_fields[SWAP_TOTAL].value - meminfo_fields[SWAP_FREE].value);
    swap_usage /= meminfo_fields[SWAP_TOTAL].value;
	
    // report data
    int ret = nprobe_fprintf(stdout, "|%s|%s|%llu|%llu|%llu|%.2f|%llu|%llu|%llu|%llu|%llu|%llu|%.2f|\n",
        METRICS_MEMINFO_NAME,
        METRICS_MEMINFO_PATH,
        meminfo_fields[MEM_TOTAL].value,
        meminfo_fields[MEM_FREE].value,
        meminfo_fields[MEM_AVAILABLE].value,
        mem_usage,
        meminfo_fields[BUFFERS].value,
        meminfo_fields[CACHED].value,
        meminfo_fields[ACTIVE].value,
        meminfo_fields[INACTIVE].value,
        meminfo_fields[SWAP_TOTAL].value,
        meminfo_fields[SWAP_FREE].value,
        swap_usage);
    if (ret < 0) {
        return -1;
    }
    return 0;
}

// probes
int system_meminfo_probe()
{
    FILE* f = NULL;
    char line[LINE_BUF_LEN];
    int ret = 0;

    f = fopen(METRICS_MEMINFO_PATH, "r");
    if (f == NULL) {
        return -1;
    }
    int cur_index = 0;
    while (!feof(f)) {
        if (fgets(line, LINE_BUF_LEN, f) == NULL) {
            break;
        }
        ret = get_meminfosp_fileds(line, cur_index);
        if (!ret) {
            cur_index++;
        }

        // reading file ends when index = TOTAL_DATA_INDEX
        if (cur_index == TOTAL_DATA_INDEX) {
            break;
        }
    }
	
    (void)fclose(f);
    ret = output_info();
    if (ret < 0) {
        return -1;
    }
    return 0;
}
